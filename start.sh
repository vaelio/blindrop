#!/bin/sh

/brop3 &
PID=$!

socat TCP-LISTEN:5556,fork,reuseaddr,keepidle=20 TCP:127.0.0.1:5555 &
echo $PID > brop.pid
echo "Challenge exposed on port 5556"

while true
do
    ps --ppid $PID -o pid,time,cmd | grep brop3 | grep -v "00:00:0" | awk '{print $1}' | xargs kill -9 - 2>/dev/null
    sleep 1
done