FROM archlinux:base-20210120.0.13969

RUN pacman -Syu --noconfirm && pacman -Sy --noconfirm socat
ADD brop3 /brop3
ADD start.sh /start.sh
RUN chmod +x /start.sh brop3
EXPOSE 5556
ENTRYPOINT [ "/start.sh" ]
